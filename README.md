# Docker Compose Templates

This is where I'll keep the various docker-compose templates I use so that I don't lose them. I'll also keep track of any changes I might have made so that others can benefit.

## Services:
- SWAG: Web Proxy
- Plex: Media Server
- Tautulli: Metrics for Plex
- Homebridge: Bridge for Alarm.com
- Nextcloud: Personal cloud storage
- Mariadb: Database for Nextcloud